import { ErrorHandler, Injectable } from '@angular/core';
import { LogsService } from './logs.service';

@Injectable()
export class LogsHandler implements ErrorHandler {

  constructor(private logsService: LogsService) { }

  handleError(error: Error): void {
    this.logsService.error(error.message, error.stack);
    console.error(error);
  }

}
