import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { publish } from 'rxjs/operators';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class LogsService {

  constructor(private http: HttpClient) { }

  error(message: string, trace?: string, context?: string): void {
    publish()(this.http.post(`${environment.domains.mel}/api/logs`, {
      message, trace, context, type: 'error'
    })).connect();
  }

  warn(message: string, context?: string): void {
    publish()(this.http.post(`${environment.domains.mel}/api/logs`, {
      message, context, type: 'warning'
    })).connect();
  }
}
