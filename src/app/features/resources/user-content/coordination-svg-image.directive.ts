import { Directive, HostBinding, HostListener, Input } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Directive({
  selector: '[appCoordinationSvgImage]'
})
export class CoordinationSvgImageDirective {

  @Input() clipPathRect!: any;
  @Input() set image$(obs: Observable<string> | undefined) {
    this.unsubscriber.next();
    this.photo = undefined;
    if (obs) {
      obs.pipe(takeUntil(this.unsubscriber)).subscribe({ next: url => this.photo = url });
      // CORS issue for now, waiting for custom image storate
      // obs.pipe(takeUntil(this.unsubscriber)).subscribe({
      //   next: url => {
      //     const tmpImage = new Image();
      //     tmpImage.onload = () => {
      //       const canvas = document.createElement('canvas');
      //       const ctx = canvas.getContext('2d');
      //       canvas.width = tmpImage.width;
      //       canvas.height = tmpImage.height;
      //       ctx?.drawImage(tmpImage, 0, 0);
      //       this.photo = canvas.toDataURL();
      //     };
      //     tmpImage.crossOrigin = 'Anonymous';
      //     tmpImage.src = url;
      //   }
      // });
    }
  }

  photo?: string;

  @HostBinding('attr.width')
  width!: number;

  @HostBinding('attr.height')
  height!: number;

  private scaleRatio!: number;
  private unsubscriber = new Subject();

  @HostBinding('attr.x') x = '50%';
  @HostBinding('attr.y') y = '50%';

  @HostBinding('attr.transform') get transform(): string | undefined {
    if (this.width && this.height) {
      return `translate(${this.width / -2}, ${this.height / -2})`;
    }
    return undefined;
  }

  @HostBinding('attr.href') get href(): string {
    return this.photo || '';
  }

  constructor() { }

  @HostListener('load', ['$event'])
  onImageLoad(event: any): void {
    const imgBBox = ((event.path && event.path[0] || event.composedPath()[0]) as SVGImageElement).getBBox();
    const clipPath = this.clipPathRect.getBBox();
    const scaleRatio = clipPath.width > clipPath.height ? clipPath.width / imgBBox.width : clipPath.height / imgBBox.height;
    this.scaleRatio = scaleRatio + 0.1;
    this.width = imgBBox.width * this.scaleRatio;
    this.height = imgBBox.height * this.scaleRatio;
    this.x = clipPath.x + (clipPath.width / 2);
    this.y = clipPath.y + (clipPath.height / 2);
  }

}
