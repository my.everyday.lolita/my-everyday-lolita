import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Criterium, Item, ItemsService, SharedDataResolver } from '@lheido/mel-shared';
import { Observable } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';
import { UserContentService } from './user-content.service';

@Injectable({ providedIn: 'root' })
export class ShareItemsResolver implements Resolve<any[]> {
  constructor(
    private shareResolver: SharedDataResolver,
    private itemsService: ItemsService,
    private userContentService: UserContentService
  ) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): any[] | Observable<any[]> | Promise<any[]> {
    return this.shareResolver.resolve(route, state).pipe(
      switchMap(data => {
        const criteria: Criterium[] = data.map(item => ({ type: 'id', value: item.id.split(':')[0] }));
        return this.itemsService.findByCriteria(criteria).pipe(
          map(result => {
            return result.reduce((acc, item) => {
              item.variants.forEach((variant, index) => {
                const variantId = this.userContentService.buildVariantId(item, index);
                if (!!data.find(i => i.id === variantId)) {
                  const clone = JSON.parse(JSON.stringify(item)) as Item;
                  const y = item.variants.findIndex(v => v === variant);
                  acc.push({ ...clone, variants: [clone.variants[y]] });
                }
              });
              return acc;
            }, [] as Item[]);
          })
        );
      })
    );
  }
}
