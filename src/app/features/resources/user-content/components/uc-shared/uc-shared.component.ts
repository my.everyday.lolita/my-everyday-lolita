import { Component, ContentChild, Input, OnInit, TemplateRef } from '@angular/core';
import { Item } from '@lheido/mel-shared';
import { PaginationInstance } from 'ngx-pagination';
import { EmptyResultDirective } from '../../../items/empty-result.directive';
import { ItemTemplateDirective } from '../../../items/item-template.directive';

@Component({
  selector: 'app-uc-shared, [uc-shared]',
  templateUrl: './uc-shared.component.html',
  styleUrls: ['./uc-shared.component.scss']
})
export class UcSharedComponent implements OnInit {

  @ContentChild(ItemTemplateDirective, { static: true, read: TemplateRef }) itemTemplate!: TemplateRef<any>;
  @ContentChild(EmptyResultDirective, { static: true, read: TemplateRef }) emptyTemplate!: TemplateRef<any>;

  @Input() items: Item[] = [];

  paginationConfig: PaginationInstance = {
    id: 'mel-pager',
    itemsPerPage: 20,
    currentPage: 1
  };

  constructor() { }

  ngOnInit(): void {
  }

  trackByFn(index: number, item: Item): string {
    return item._variantId as string;
  }

  onPageChange(page: number): void {
    this.paginationConfig.currentPage = page;
  }

}
