import { Component, Input } from '@angular/core';
import { Item } from '@lheido/mel-shared';
import { UserContentService } from '../../user-content.service';

@Component({
  selector: 'app-uc-wishlist',
  templateUrl: './uc-wishlist.component.html',
})
export class UcWishlistComponent {

  @Input() item!: Item;

  constructor(
    private userContentService: UserContentService
  ) { }

  toggleDreamDressProperty(): void {
    this.item.dreamDress = this.userContentService.toggleDreamDressPropertyWishlist(this.item);
  }

  remove(): void {
    this.userContentService.removeFromWishlist(this.item);
  }

}
