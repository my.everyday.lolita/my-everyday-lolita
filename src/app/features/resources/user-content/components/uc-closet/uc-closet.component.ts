import { Component, Input } from '@angular/core';
import { Item } from '@lheido/mel-shared';
import { UserContentService } from '../../user-content.service';

@Component({
  selector: 'app-uc-closet',
  templateUrl: './uc-closet.component.html',
})
export class UcClosetComponent {

  @Input() item!: Item;

  constructor(
    private userContentService: UserContentService
  ) { }

  toggleWantToSellProperty(): void {
    this.item.wantToSell = this.userContentService.toggleWanToSellPropertyCloset(this.item);
  }

  remove(): void {
    this.userContentService.removeFromCloset(this.item);
  }

}
