import { BreakpointObserver } from '@angular/cdk/layout';
import { Component, ContentChild, Input, OnDestroy, OnInit, TemplateRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Criterium, Item } from '@lheido/mel-shared';
import { PaginationInstance } from 'ngx-pagination';
import { from, Observable, of, Subject, zip } from 'rxjs';
import { filter, map, switchMap, takeUntil } from 'rxjs/operators';
import { CacheService } from 'src/app/features/cache/cache.service';
import { EmptyResultDirective } from '../../../items/empty-result.directive';
import { ItemTemplateDirective } from '../../../items/item-template.directive';
import { UserContentClosetItem, UserContentWishlistItem } from '../../user-content.model';
import { UserContentService } from '../../user-content.service';

@Component({
  selector: 'app-uc-items, [uc-items]',
  templateUrl: './uc-items.component.html',
  styleUrls: ['./uc-items.component.scss'],
  exportAs: 'ucItems'
})
export class UcItemsComponent implements OnInit, OnDestroy {

  @Input() field!: 'closet' | 'wishlist';

  @ContentChild(ItemTemplateDirective, { static: true, read: TemplateRef }) itemTemplate!: TemplateRef<any>;
  @ContentChild(EmptyResultDirective, { static: true, read: TemplateRef }) emptyTemplate!: TemplateRef<any>;

  items: Item[] = [];
  results: Item[] = [];
  content: (UserContentWishlistItem | UserContentClosetItem)[] = [];
  nbItems = 0;
  totalEstimatedPrice = 0;
  paginationConfig: PaginationInstance = {
    id: 'mel-pager',
    itemsPerPage: 20,
    currentPage: 1
  };
  lastSearch?: string;
  lastSort?: string;
  lastLength?: number;
  selectedCriteria: Criterium[] = [];

  private unsubscriber = new Subject();
  private breakpoints = ['(min-width: 700px)', '(min-width: 900px)', '(min-width: 1367px)'];
  private breakpointsItemsPerPageMap: { [key: string]: number } = {
    '(min-width: 700px)': 10,
    '(min-width: 900px)': 10,
    '(min-width: 1367px)': 10,
  };

  constructor(
    private userContentService: UserContentService,
    private cacheService: CacheService,
    private breakpointObserver: BreakpointObserver,
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) { }

  ngOnInit(): void {
    this.userContentService.content$.pipe(
      filter(content => content !== null),
      switchMap(content => {
        this.content = content[this.field];
        this.nbItems = this.content.length;
        return this.getItems();
      }),
      map(items => {
        this.items = items;
        this.totalEstimatedPrice = this.items.map(item => item.estimatedPrice || 0).reduce((count, value) => count + value, 0);
      }),
      switchMap(() => this.activatedRoute.queryParams),
      takeUntil(this.unsubscriber)
    ).subscribe(params => {
      if (
        (this.lastLength && this.items.length !== this.lastLength) ||
        (this.lastSearch && JSON.stringify(params.criteria || []) !== this.lastSearch) ||
        (this.lastSort && params.sort || 'alpha_asc' !== this.lastSort)
      ) {
        this.selectedCriteria = params.criteria && JSON.parse(params.criteria) || [];
        this.lastSearch = JSON.stringify(this.selectedCriteria);
        this.lastSort = params.sort || 'alpha_asc';
        this.lastLength = this.items.length;
        this.results = this.filterItems(this.selectedCriteria, params.sort);
        this.paginationConfig.currentPage = 1;
      }
    });

    this.breakpointObserver.observe(this.breakpoints).pipe(takeUntil(this.unsubscriber)).subscribe({
      next: result => {
        const itemsPerPage = Object.entries(result.breakpoints).reduce((acc, [breakpoint, value]) => {
          if (value) {
            acc += this.breakpointsItemsPerPageMap[breakpoint];
          }
          return acc;
        }, 20);
        if (this.paginationConfig.itemsPerPage !== itemsPerPage) {
          this.paginationConfig.itemsPerPage = itemsPerPage;
          this.router.navigate([], { queryParams: { ...this.activatedRoute.snapshot.queryParams, page: 1 }, replaceUrl: true });
        }
      }
    });
  }

  ngOnDestroy(): void {
    this.unsubscriber.next();
    this.unsubscriber.complete();
  }

  trackByFn(index: number, item: Item): string {
    return item._variantId as string;
  }

  onPageChange(page: number): void {
    this.paginationConfig.currentPage = page;
  }

  private getItems(): Observable<Item[]> {
    const items$ = this.content.filter(item => !item._wrongVariantId).map(item => this.cacheService.match(item.id).pipe(
      switchMap(cache => cache ? from(cache?.json()) : of(undefined)),
      map((almostReadyitem: Item) => {
        if (almostReadyitem) {
          if (this.field === 'closet') {
            almostReadyitem.wantToSell = (item as UserContentClosetItem).wantToSell;
          } else if (this.field === 'wishlist') {
            almostReadyitem.dreamDress = (item as UserContentWishlistItem).dreamDress;
          }
        }
        return almostReadyitem;
      })
    ));
    if (items$.length === 0) {
      return of([]);
    }
    return zip(...items$).pipe(
      map(items => items.filter(i => i !== undefined))
    );
  }

  private filterItems(criteria: Criterium[], sort: string): Item[] {
    const brandCriteria = criteria.filter(crit => crit.type === 'brand');
    const categoryCriteria = criteria.filter(crit => crit.type === 'category');
    const filteredItems = this.items.filter(item => {
      let viewByBrand = brandCriteria.length === 0;
      if (brandCriteria.length > 0) {
        viewByBrand = brandCriteria.map(crit => crit.value).includes(item.brand.name);
      }
      let viewByCat = categoryCriteria.length === 0;
      if (categoryCriteria.length > 0) {
        viewByCat = categoryCriteria.map(crit => crit.value).includes(item.category.name);
        if (!viewByCat && item.category.parent) {
          viewByCat = categoryCriteria.map(crit => crit.value).includes(item.category.parent.name);
          if (!viewByCat && item.category.parent.parent) {
            viewByCat = categoryCriteria.map(crit => crit.value).includes(item.category.parent.parent.name);
          }
        }
      }
      return viewByBrand && viewByCat;
    });
    filteredItems.sort((a, b) => {
      let result: number;
      let aPrice: number;
      let bPrice: number;
      let aBrand: string;
      let bBrand: string;
      let aCollection: string;
      let bCollection: string;
      switch (sort) {
        case 'price_asc':
          aPrice = a.estimatedPrice || 0;
          bPrice = b.estimatedPrice || 0;
          result = aPrice > bPrice ? 1 : aPrice < bPrice ? -1 : 0;
          break;

        case 'price_desc':
          aPrice = a.estimatedPrice || 0;
          bPrice = b.estimatedPrice || 0;
          result = aPrice > bPrice ? -1 : aPrice < bPrice ? 1 : 0;
          break;

        case 'year_asc':
          aPrice = a.year || 0;
          bPrice = b.year || 0;
          result = aPrice > bPrice ? 1 : aPrice < bPrice ? -1 : 0;
          break;

        case 'year_desc':
          aPrice = a.year || 0;
          bPrice = b.year || 0;
          result = aPrice > bPrice ? -1 : aPrice < bPrice ? 1 : 0;
          break;

        case 'alpha_asc':
          aBrand = a.brand.name.toLowerCase();
          aCollection = (a.collectionn || '').toLowerCase();
          bBrand = b.brand.name.toLowerCase();
          bCollection = (b.collectionn || '').toLowerCase();
          if (aBrand === bBrand) {
            result = aCollection > bCollection ? 1 : aCollection < bCollection ? -1 : 0;
          } else {
            result = aBrand > bBrand ? 1 : aBrand < bBrand ? -1 : 0;
          }
          break;

        case 'alpha_desc':
          aBrand = a.brand.name.toLowerCase();
          aCollection = (a.collectionn || '').toLowerCase();
          bBrand = b.brand.name.toLowerCase();
          bCollection = (b.collectionn || '').toLowerCase();
          if (aBrand === bBrand) {
            result = aCollection > bCollection ? -1 : aCollection < bCollection ? 1 : 0;
          } else {
            result = aBrand > bBrand ? -1 : aBrand < bBrand ? 1 : 0;
          }
          break;

        default:
          result = 0;
          break;
      }
      return result;
    });
    return filteredItems;
  }

}
