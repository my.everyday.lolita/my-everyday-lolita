import { Directive, HostBinding, HostListener, Input, OnInit } from '@angular/core';
import { ShareableData, UserService } from '@lheido/mel-shared';
import { TranslateService } from '@ngx-translate/core';
import { UserContentService } from './user-content.service';

@Directive({
  selector: '[appShare], [app-share]'
})
export class ShareDirective implements OnInit {

  @Input() key!: ShareableData;

  @HostBinding('disabled') disabled = navigator.share === undefined;

  constructor(
    private userContentService: UserContentService,
    private translateService: TranslateService,
    private userService: UserService
  ) { }

  ngOnInit(): void {
    if (!this.userService.user) {
      this.disabled = true;
    }
  }

  @HostListener('click')
  onClick(): void {
    const id = this.userContentService.content._id;
    const pseudo = this.userService.user?.preferred_username;
    const translateParams = {
      pseudo,
      label: this.key === 'closet' ? 'closet' : 'wishlist',
    };
    this.translateService.get('SHARE.TITLE', translateParams).subscribe(title => {
      navigator.share({
        title,
        url: `${location.origin}/share/${id}/${this.key}?pseudo=${pseudo}`
      });
    });
  }

}
