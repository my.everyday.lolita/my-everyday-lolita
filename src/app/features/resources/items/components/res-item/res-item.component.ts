import { Component, HostBinding, Input } from '@angular/core';
import { Item } from '@lheido/mel-shared';

@Component({
  selector: 'app-res-item, [res-item]',
  templateUrl: './res-item.component.html',
})
export class ResItemComponent {

  @HostBinding('class.item') classItem = true;

  @Input() item!: Item;

}
