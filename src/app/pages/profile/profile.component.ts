import { trigger, transition, style, animate } from '@angular/animations';
import { Component, HostBinding, OnDestroy, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { saveAs } from 'file-saver';
import { UserContentService } from 'src/app/features/resources/user-content/user-content.service';
import { User } from 'src/app/features/user/user.model';
import { UserService } from 'src/app/features/user/user.service';
import { ToastrService } from 'ngx-toastr';
import { TranslateService } from '@ngx-translate/core';
import { FormControl, Validators } from '@angular/forms';
import { environment } from 'src/environments/environment';
import { SearchComponent } from '../search/search.component';

@Component({
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss'],
  animations: [
    trigger('pageAnimation', [
      transition(':enter', [
        style({ opacity: 0 }),
        animate('330ms 330ms linear', style({ opacity: 1 }))
      ]),
      transition(':leave', [
        style({ opacity: 1, transform: 'translateY(0%)' }),
        animate('330ms linear', style({ opacity: 0, transform: 'translateY(5%)' }))
      ]),
    ])
  ]
})
export class ProfileComponent implements OnInit, OnDestroy {

  @HostBinding('@pageAnimation') private pageAnimation = true;

  user: User | undefined;
  userContentDownloadLink?: string;
  searchLimitControl = new FormControl(
    localStorage.getItem(SearchComponent.SEARCH_LIMIT_KEY) || 500,
    [Validators.min(0), Validators.max(500)]
  );
  enableSearchLimit = environment.config.searchLimit;
  localStorageSize: string;

  private unsubscriber = new Subject();

  constructor(
    private userService: UserService,
    private userContent: UserContentService,
    private toastr: ToastrService,
    private translate: TranslateService
  ) {
    this.localStorageSize = this.getLocalStorageSize();
    try {
      console.info(`local storage size: ${this.localStorageSize} KB`);
    } catch (error) {
      console.debug(`local storage size: ${this.localStorageSize} KB`);
    }
  }

  ngOnDestroy(): void {
    this.unsubscriber.next();
    this.unsubscriber.complete();
  }

  ngOnInit(): void {
    this.user = this.userService.user;
    this.searchLimitControl.valueChanges.pipe(takeUntil(this.unsubscriber)).subscribe({
      next: value => {
        if (value > 0 && value <= 500) {
          localStorage.setItem(SearchComponent.SEARCH_LIMIT_KEY, value);
        }
      }
    });
  }

  export(): void {
    this.userContent.content$.pipe(takeUntil(this.unsubscriber)).subscribe({
      next: content => {
        const data = new Blob([JSON.stringify(content)], { type: 'text/plain;charset=utf-8' });
        saveAs(data, 'mel-user-content.json');
      }
    });
  }

  async onImport(event: Event): Promise<void> {
    const file = (event.target as HTMLInputElement).files?.item(0);
    if (file) {
      const loadingText = await this.translate.get('PROFILE.LOADING_TEXT').toPromise();
      this.toastr.info(loadingText, undefined, { disableTimeOut: true, closeButton: false });
      const content = await file.text();
      this.userContent.setContent(JSON.parse(content)).subscribe({
        next: async result => {
          if (result) {
            this.toastr.clear();
            const successText = await this.translate.get('PROFILE.IMPORT.SUCCESS').toPromise();
            this.toastr.success(successText);
          } else {
            const errorText = await this.translate.get('PROFILE.IMPORT.ERROR').toPromise();
            this.toastr.error(errorText);
          }
        }
      });
    }
  }

  /**
   * From https://stackoverflow.com/a/15720835
   * @returns string the local storage size in KB.
   */
  private getLocalStorageSize(): string {
    let lsTotal = 0;
    let xLen;
    let x;
    for (x in localStorage) {
      if (!localStorage.hasOwnProperty(x)) {
        continue;
      }
      xLen = ((localStorage[x].length + x.length) * 2);
      lsTotal += xLen;
    }
    return (lsTotal / 1024).toFixed(2);
  }

}
