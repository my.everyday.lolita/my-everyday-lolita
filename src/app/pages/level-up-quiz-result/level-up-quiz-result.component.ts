import { trigger, transition, style, animate } from '@angular/animations';
import { Component, HostBinding, OnDestroy, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { DialogService } from 'src/app/features/dialog/dialog.service';
import { DialogComponent } from 'src/app/features/dialog/dialog/dialog.component';
import { UserContentService } from 'src/app/features/resources/user-content/user-content.service';
import { ThemeService } from 'src/app/features/theme/theme.service';

@Component({
  templateUrl: './level-up-quiz-result.component.html',
  styleUrls: ['./level-up-quiz-result.component.scss'],
  animations: [
    trigger('pageAnimation', [
      transition(':enter', [
        style({ opacity: 0 }),
        animate('330ms 330ms linear', style({ opacity: 1 }))
      ]),
      transition(':leave', [
        style({ opacity: 1, transform: 'translateY(0%)' }),
        animate('330ms linear', style({ opacity: 0, transform: 'translateY(1%)' }))
      ]),
    ])
  ]
})
export class LevelUpQuizResultComponent implements OnInit, OnDestroy {

  @ViewChild('resetResultsModal') resetResultsModal!: TemplateRef<any>;
  @HostBinding('@pageAnimation') private pageAnimation = true;

  gaugeValue = 0;
  lang?: string;
  resultTextIndex?: number;
  score?: number;
  colorScheme: { domain: string[] };
  chartData?: any[];
  xAxisTickFormatting: (value: string) => string;
  yAxisTickFormatting: (value: string) => string;
  displayChart = false;

  private unsubscriber = new Subject();

  constructor(
    private userContentService: UserContentService,
    private translateService: TranslateService,
    private dialogService: DialogService,
    private router: Router,
    private themeService: ThemeService
  ) {
    this.xAxisTickFormatting = (value: string): string => {
      const date = (typeof value === 'string' ? new Date(value) : value);
      return date.toLocaleDateString(this.translateService.currentLang || this.translateService.defaultLang);
    };
    this.yAxisTickFormatting = (value: string): string => {
      return parseInt(value, 10).toFixed();
    };

    this.colorScheme = {
      domain: [
        this.getChartLineColor(),
        '#000'
      ]
    };
    this.themeService.theme$.pipe(takeUntil(this.unsubscriber)).subscribe({
      next: _ => {
        this.colorScheme = {
          domain: [
            this.getChartLineColor(),
            '#000'
          ]
        };
      }
    });
  }

  ngOnInit(): void {
    this.lang = this.translateService.currentLang || this.translateService.defaultLang;
    this.translateService.onLangChange.pipe(takeUntil(this.unsubscriber)).subscribe({
      next: event => {
        this.lang = event.lang;
      }
    });
    const result = this.userContentService.content.levelUpQuiz;
    this.displayChart = result.length > 1;
    if (result.length > 0) {
      const lastResult = result[result.length - 1];
      this.score = lastResult.items.reduce((count, value) => value ? ++count : count, 0);
      this.gaugeValue = this.score / 100;
      if (this.score >= 0 && this.score <= 15) {
        this.resultTextIndex = 0;
      } else if (this.score >= 16 && this.score <= 30) {
        this.resultTextIndex = 1;
      } else if (this.score >= 31 && this.score <= 50) {
        this.resultTextIndex = 2;
      } else if (this.score >= 51 && this.score <= 65) {
        this.resultTextIndex = 3;
      } else if (this.score >= 66 && this.score <= 85) {
        this.resultTextIndex = 4;
      } else {
        this.resultTextIndex = 5;
      }
      this.chartData = [{
        name: 'Results',
        series: result.map(r => ({
          value: r.items.reduce((count, value) => value ? ++count : count, 0),
          name: r.date
        }))
      }];
    }
  }

  ngOnDestroy(): void {
    this.unsubscriber.next();
    this.unsubscriber.complete();
  }

  reset(): void {
    this.dialogService.open(this.resetResultsModal, { modal: true });
  }

  resetResults(dialog: DialogComponent): void {
    this.userContentService.content.levelUpQuiz = [];
    this.userContentService.markAsChanged();
    dialog.close();
    this.router.navigateByUrl('/tips/level-up-quiz');
  }

  private getChartLineColor(): string {
    return getComputedStyle(document.body).getPropertyValue('--line-chart-line-path').trim();
  }

}
