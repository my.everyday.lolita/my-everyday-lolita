import { trigger, transition, style, animate } from '@angular/animations';
import { Component, HostBinding, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { merge } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { UserContentService } from 'src/app/features/resources/user-content/user-content.service';

@Component({
  templateUrl: './level-up-quiz.component.html',
  styleUrls: ['./level-up-quiz.component.scss'],
  animations: [
    trigger('pageAnimation', [
      transition(':enter', [
        style({ opacity: 0 }),
        animate('330ms 330ms linear', style({ opacity: 1 }))
      ]),
      transition(':leave', [
        style({ opacity: 1, transform: 'translateY(0%)' }),
        animate('330ms linear', style({ opacity: 0, transform: 'translateY(1%)' }))
      ]),
    ])
  ]
})
export class LevelUpQuizComponent implements OnInit {

  @HostBinding('@pageAnimation') private pageAnimation = true;

  items: string[] = [];
  form?: FormGroup;
  haveLastResult?: boolean;

  constructor(
    private translateService: TranslateService,
    private fb: FormBuilder,
    private router: Router,
    private userContentService: UserContentService
  ) { }

  ngOnInit(): void {
    merge(
      this.translateService.get('LEVEL_UP_QUIZ.QUIZ'),
      this.translateService.onLangChange.pipe(switchMap(() => this.translateService.get('LEVEL_UP_QUIZ.QUIZ'))),
    ).subscribe({
      next: (items: string[]) => {
        this.items = items;
        const results = this.userContentService.content.levelUpQuiz;
        const lastResult = results.length > 0 ? results[results.length - 1] : null;
        this.haveLastResult = lastResult !== null;
        if (!this.form) {
          this.form = this.fb.group({
            date: new Date(),
            items: this.fb.array(items.map((_, i) => {
              let initialValue = false;
              if (lastResult && lastResult.items[i] !== undefined) {
                initialValue = lastResult.items[i];
              }
              return this.fb.control(initialValue);
            }))
          });
        }
      }
    });
  }

  onStarClick(i: number): void {
    const control = ((this.form?.get('items') as FormArray).get(`${i}`) as FormControl);
    control.setValue(!control.value);
  }

  onSubmit(): void {
    this.userContentService.addLevelUpQuizResult(this.form?.value);
    this.router.navigateByUrl('/tips/level-up-quiz/result');
  }

}
