import { trigger, transition, style, animate } from '@angular/animations';
import { Component, HostBinding } from '@angular/core';
import { ThemeService } from 'src/app/features/theme/theme.service';

@Component({
  templateUrl: './my-wishlist.component.html',
  styleUrls: ['./my-wishlist.component.scss'],
  animations: [
    trigger('pageAnimation', [
      transition(':enter', [
        style({ opacity: 0 }),
        animate('330ms linear', style({ opacity: 1 }))
      ]),
      transition(':leave', [
        style({ opacity: 1, transform: 'translateY(0%)' }),
        animate('330ms linear', style({ opacity: 0, transform: 'translateY(5%)' }))
      ]),
    ])
  ]
})
export class MyWishlistComponent {

  @HostBinding('@pageAnimation') private animation = true;
  @HostBinding('class.my-wishlist') private hostClass = true;

  footerEE = 0;

  constructor(
    public themeService: ThemeService
  ) { }

  toggleFooterEE(): void {
    this.footerEE = (this.footerEE + 1) % 2;
  }

}
