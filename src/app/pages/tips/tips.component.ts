import { trigger, transition, style, animate } from '@angular/animations';
import { Component, HostBinding, Inject, OnInit } from '@angular/core';
import { Routes } from '@angular/router';
import { APP_GAMES_TIPS_ROUTES } from 'src/app/app.token';

@Component({
  templateUrl: './tips.component.html',
  styleUrls: ['./tips.component.scss'],
  animations: [
    trigger('pageAnimation', [
      transition(':enter', [
        style({ opacity: 0 }),
        animate('330ms 330ms linear', style({ opacity: 1 }))
      ]),
      transition(':leave', [
        style({ opacity: 1, transform: 'translateY(0%)' }),
        animate('330ms linear', style({ opacity: 0, transform: 'translateY(5%)' }))
      ]),
    ])
  ]
})
export class TipsComponent implements OnInit {

  @HostBinding('@pageAnimation') private pageAnimation = true;

  constructor(@Inject(APP_GAMES_TIPS_ROUTES) public routes: Routes) { }

  ngOnInit(): void {
  }

}
