import { trigger, transition, style, animate } from '@angular/animations';
import { Component, HostBinding, OnDestroy, OnInit, SecurityContext } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { ActivatedRoute } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { Subject } from 'rxjs';
import { startWith, takeUntil } from 'rxjs/operators';
import { TitleService } from 'src/app/features/title/title.service';

@Component({
  templateUrl: './share.component.html',
  styleUrls: ['./share.component.scss'],
  animations: [
    trigger('pageAnimation', [
      transition(':enter', [
        style({ opacity: 0 }),
        animate('330ms linear', style({ opacity: 1 }))
      ]),
      transition(':leave', [
        style({ opacity: 1, transform: 'translateY(0%)' }),
        animate('330ms linear', style({ opacity: 0, transform: 'translateY(5%)' }))
      ]),
    ]),
  ]
})
export class ShareComponent implements OnInit, OnDestroy {

  @HostBinding('@pageAnimation') private animation = true;

  data: any[] = [];
  private unsubscriber = new Subject<void>();

  constructor(
    private activatedRoute: ActivatedRoute,
    private title: TitleService,
    private domSanitizer: DomSanitizer,
    private translate: TranslateService
  ) { }

  ngOnDestroy(): void {
    this.unsubscriber.next();
    this.unsubscriber.complete();
  }

  ngOnInit(): void {
    this.data = (this.activatedRoute.snapshot.data.data as any);

    this.translate.onLangChange.pipe(
      startWith({}),
      takeUntil(this.unsubscriber)
    ).subscribe({
      next: () => {
        this.updateTitle();
      }
    });
  }

  private updateTitle(): void {
    setTimeout(() => {
      if (this.activatedRoute.snapshot.queryParams.pseudo) {
        const typeLabel = this.activatedRoute.snapshot.params.key === 'closet' ? 'closet' : 'wishlist';
        const translateParams = {
          pseudo: this.activatedRoute.snapshot.queryParams.pseudo,
          label: typeLabel
        };
        this.translate.get('SHARE.TITLE', translateParams).subscribe({
          next: value => {
            this.title.set(this.domSanitizer.sanitize(SecurityContext.HTML, value) as string, true);
          }
        });
      } else {
        const typeLabel = this.activatedRoute.snapshot.params.key === 'closet' ? 'Closet' : 'Wishlist';
        this.title.set(typeLabel, true);
      }
    });
  }

}
